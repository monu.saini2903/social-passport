// config/passport.js

// load all the things we need
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth2').Strategy;

// load up the user model
var User = require('./user');
var jwt = require('jsonwebtoken');

// load the auth variables
var configAuth = require('./oauth');

module.exports = function (passport) {
    console.log('PASSPRT CALLED')
    // =========================================================================
    // FACEBOOK ================================================================
    // =========================================================================
    // used to serialize the user for the session
    passport.use(new FacebookStrategy({
        clientID: configAuth.facebookAuth.clientID,
        clientSecret: configAuth.facebookAuth.clientSecret,
        callbackURL: configAuth.facebookAuth.callbackURL
    },
        function (accessToken, refreshToken, profile, done) {
            User.findOne({ oauthID: profile.id }, function (err, user) {
                if (err)
                    done(err, null);
                if (!err && user !== null) {
                    var token = jwt.sign(user, 'foobar', {
                        expiresIn: 10080 // in seconds
                    });
                    done(null, { success: true, token: token, prefix: 'JWT' }); // user found, return that user
                } else {
                    user = new User({
                        oauthID: profile.id,
                        name: profile.displayName,
                        created: Date.now()
                    });
                    user.save(function (err) {
                        if (err) {
                            console.log(err);  // handle errors!
                        } else {
                            console.log("saving user ...");
                            var token = jwt.sign(user, 'foobar', {
                                expiresIn: 10080 // in seconds
                            });
                            done(null, { success: true, token: token, prefix: 'JWT' });
                        }
                    });
                }
            });
        }
    ));

    passport.use(new GoogleStrategy({
        clientID: configAuth.google.clientID,
        clientSecret: configAuth.google.clientSecret,
        callbackURL: configAuth.google.callbackURL
    },
        function (request, accessToken, refreshToken, profile, done) {
            User.findOne({ oauthID: profile.id }, function (err, user) {
                if (err) {
                     done(err, null);  // handle errors!
                }
                if (!err && user !== null) {
                    var token = jwt.sign(user, 'foobar', {
                        expiresIn: 10080 // in seconds
                    });
                    done(null, { success: true, token: token, prefix: 'JWT' }); // user found, return that user
                } else {
                    user = new User({
                        oauthID: profile.id,
                        name: profile.displayName,
                        created: Date.now()
                    });
                    user.save(function (err) {
                        if (err) {
                            console.log(err);  // handle errors!
                        } else {
                            console.log("saving user ...");
                            var token = jwt.sign(user, 'foobar', {
                                expiresIn: 10080 // in seconds
                            });
                            done(null, { success: true, token: token, prefix: 'JWT' });
                        }
                    });
                }
            });
        }
    ));

};