var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var index = require('./routes/index');
var users = require('./routes/users');
var passport = require('passport'),
    session = require('express-session'),
    mongoStore = require('connect-mongo')(session);
var mongoose = require('mongoose');

var app = express();
// use passport session

// Express MongoDB session storage
/*app.use(session({
    saveUninitialized: true,
    resave: true,
    secret: 'foobar',
    store: new mongoStore({
        mongooseConnection: mongoose.connection,
        collection: 'session'
    })
}));*/

mongoose.connect('mongodb://localhost:27017/ba');

var timestamps = require('mongoose-timestamp');

mongoose.plugin(timestamps,  {
  createdAt: 'created_at',
  updatedAt: 'modified_at'
})

app.use(passport.initialize());

require('./passport')(passport)


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use('/users', users);

app.get('/profile', isLoggedIn, function (req, res) {
  res.send({message: 'user authenticated successfully'})
});

app.get('/auth/facebook',
  passport.authenticate('facebook',{ scope : 'email' }),
  function(req, res){
    console.log('Error', error);
    console.log('Res', res);
  });
app.get('/auth/facebook/callback',
  passport.authenticate('facebook', { failureRedirect: '/' ,session: false}),
  function(req, res) {
    res.redirect('/profile');
  });


app.get('/auth/google',
  passport.authenticate('google', { scope: [
    'https://www.googleapis.com/auth/plus.login',
    'https://www.googleapis.com/auth/plus.profile.emails.read'
  ] }
));

app.get('/auth/google/callback',
  passport.authenticate('google', { failureRedirect: '/' ,session: false}),
  function(req, res) {
    res.redirect('/profile');
  });

// route middleware to make sure a user is logged in
function isLoggedIn(req, res, next) {

    // if user is authenticated in the session, carry on
    if (passport.authenticate('jwt', { session: false }))
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/');
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
